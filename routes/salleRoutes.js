const express = require('express');
const router = express.Router();
const connection = require('../config/db');
const salleController = require('../controllers/salleController');

router.get('/salles', salleController.getSalle);
router.get('/incidents', salleController.getSalle_incident);

router.delete('/incident/:id', salleController.deleteIncident);

router.post('/incidentajout', salleController.ajouterIncident);

module.exports = router;