const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.post('/users', userController.postUsers);
router.post('/login', userController.login);
router.post('/login-bureautique', userController.loginBureautique);

module.exports = router;
