const express = require('express');
const router = express.Router();
const connection = require('../config/db');
const mysql = require('mysql2');
const espaceUtilisateurController = require('../controllers/espaceUtilisateurController');

router.get('/espace-utilisateur', espaceUtilisateurController.getEspaceUtilisateur);
router.get('/espace-utilisateur-mobile/:email_utilisateur', espaceUtilisateurController.getEspaceUtilisateur_mobile);
router.post('/ajouter-avis', espaceUtilisateurController.ajouterAvis);
module.exports = router;