const connection = require('../config/db');

exports.getSalle = (req, res) => {
  connection.query('SELECT * FROM salle', (error, results) => {
    if (error) {
      return res.status(500).send('erreur de récup salle');
    }
    res.json(results);
  });
};

exports.getSalle_incident = (req, res) => {
  connection.query('SELECT * FROM vue_incidents', (error, results) => {
    if (error) {
      return res.status(500).send('erreur de récup incident salle');
    }
    res.json(results);
  });
};

exports.deleteIncident = (req, res) => {
  const id_incident = req.params.id;
  const query = 'DELETE FROM incident WHERE id_incident = ?';
  connection.query(query, [id_incident], (error, results) => {
    if (error) {
      return res.status(500).send(`Erreur lors de la suppression de l'incident avec l'ID ${id_incident}`);
    }
    res.send(`Incident avec l'ID ${id_incident} supprimé avec succès`);
  });
};

exports.ajouterIncident = (req, res) => {
  const { id_salle, commentaire } = req.body;
  if (!id_salle || !commentaire) {
    return res.status(400).json({ error: 'Les champs requis sont manquants' });
  }

  const query = 'INSERT INTO incident (id_salle, commentaire) VALUES (?, ?)'; // Correction ici
  connection.query(query, [id_salle, commentaire], (error, results) => {
    if (error) {
      console.error('Erreur lors de l\'ajout du incident:', error);
      return res.status(500).json({ error: 'Erreur lors de l\'ajout du incident' });
    }
    res.status(201).json({ message: 'Incident ajouté avec succès', id: results.insertId });
  });
};





