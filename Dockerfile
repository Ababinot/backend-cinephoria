# Utilisation de l'image node version 20.11.0 slim comme base
ARG NODE_VERSION=20.11.0
FROM node:${NODE_VERSION}-slim as base

# Réglage de l'étiquette pour la runtime de lancement sur fly.io
LABEL fly_launch_runtime="Node.js"

# Définition du répertoire de travail pour l'application Node.js
WORKDIR /app

# Configuration de l'environnement de production
ENV NODE_ENV="production"

# Étape de construction jetable pour réduire la taille de l'image finale
FROM base as build

# Installation des paquets nécessaires pour construire les modules Node.js
RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y build-essential node-gyp pkg-config python-is-python3

# Installation des dépendances Node.js
COPY package-lock.json package.json ./
RUN npm ci

# Copie du code de l'application
COPY . .

# Étape finale pour l'image de l'application
FROM base

# Copie de l'application construite à partir de l'étape de construction
COPY --from=build /app /app

# Exposition du port 3000 utilisé par l'application
EXPOSE 3000

# Commande par défaut pour démarrer le serveur (peut être remplacée au moment de l'exécution)
CMD [ "npm", "run", "start" ]
